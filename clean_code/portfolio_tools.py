def rec_tr(t, p, v, b, pf):
    '''
    Records a new trade in a portfolio.
    If the traded ticker if not already in the portfolio, it gets added.
    A "sell" decreases the portfolio position, a "buy" increases it.
    Each trade also effects the cash position in the opposite direction

       # Parameters:
       * t (string): ticker symbol of traded stock
       * p (price): price per unit of traded stock
       * v (volume): volume of the trade
       * b (bool): indicates buy or sell side (True means "buy")
       * pf (dict): portfolio containing stock and cash positions

       '''
    ss = 1
    cs = -1
    if b == True:  # For buy order, cash gets reduced and stock position increased
        ss = 1
        cs = -1

    if (t in pf.keys()) == False:  # is ticker in portfolio?
        pf[t] = 0  # Initialize position with zero

    v_new = pf[t] + ss * v  # calculate new volume
    c_new = pf["cash"] + cs * v * p  # calculate new cash position
    pf[t] = v_new  # record volume
    pf["cash"] = c_new  # adjust cash position


if __name__ == '__main__':
    my_pf = {"cash": 100000}
    rec_tr("goog", 121.3, 20, True, my_pf)
    print(my_pf)
