0. documents/
  - how_to_google.docx
    - Most important rules for searching for solutions of problems.
  - git_guide.pptx
    - Basic commands for using Git in the command line
  - VisualCode_with_WinPython_Tutorial.docx
    - Creating a new project or importing from Git
    - Configuring the correct Python environment
    - Creating a new Python file
    - Running and debugging an application
  - Pandas Cheat Sheet

1. WorkshopDay2.ipynb
  - Recap of pandas basics from day one
  - Clean Code rules
  - Clean Code examples

2. clean_code/
  - Exercise from notebook before in python file
  - Can you clean up the code following all the rules from the notebook?

3. debugging_demo/
  - Different implementations of the same functionality
  - Can you understand all of the implementations? -> Use the debugger by setting breakpoints

4. robo_trader/
  - Can you find all the bugs that are in the code?
  - Hints: 
    - function should return something like (but not exactly): 
      - [('GOOG', 'buy'), ('MRTX', 'sell')]
    - VisualCode sometimes gives hints for what could be wrong by marking lines

5. portfolio_project/
  - Now that you're able to use pandas and know some more basics of python here is your first bigger project

A trader tells you that his problem is getting many mails from analysts, that are not interesting.
Only thing of interest is whether they mention any stock the trader has in his portfolios.
He tells you that he wants to have information of a portfolio as soon as at least one of the stocks is mentioned in some analysts mails.
If a portfolio is of interest, he wants to get a mail containing following information:
- mean, standard deviation, min and max values of the whole portfolio containing the interesting stocks, of all time that is contained in the price database
- A plot of the value development of the portfolio as attachment
- The portfolios xlsx file as attachment

He wants to have a script that works somehow like this:
Get Mails -> Check if WKN is mentioned -> Filter interesting portfolios -> Get prices from database -> create plot and values -> send mail to him

From other projects he has some code snippets given in the example.py
There is code for getting the emails from his account and sending mails.
There is also code for getting the current stock prices into a local sqlite database.
! Your have to run the function "write_current_prices_for_all_stocks_in_portfolios_to_db()" at least once to get the current stock prices in a local database
! This function will create a file "stock.db" which is your local sqlite database. From day one you know how to access that data.
The folder portfolios/ contains the portfolios the trader is currently managing. They are simple xlsx files.