import pandas as pd
import re
import requests
import os 


def load_stock_watchlist(path=os.path.dirname(os.path.realpath(__file__)) + "/stock_watchlist.csv"):
    stock_watchlist = pd.read_csv(path)
    return [w.lower() for w in stock_watchlist["stocks"]]


def load_headline_text_from_www(page_num=1):
    # result = requests.get(f"https://www.nasdaq.com/news/market-headlines.aspx?page={page_num}")
    result = requests.get("https://thefly.com/news.php")
    return result.content.decode("utf-8")


def return_words_in_text(text, watchlist, max_hits=5):
    words = re.split(r"\W+", text)
    unique_words = set(words)
    stocks_in_the_headlines = []
    for word in unique_words:
        if word.upper() in watchlist:
            stocks_in_the_headlines.append(word)
        if len(stocks_in_the_headlines) == max_hits:
            break
    return stocks_in_the_headlines


def search_headline_pages_for_stock_mentions(watchlist, max_hits=5, num_pages=5):
    found_in_headlines = []
    for page_num in range(num_pages):
        text = load_headline_text_from_www(page_num)
        found_on_page = return_words_in_text(text, watchlist, max_hits-len(found_in_headlines))
        found_in_headlines.extend(found_on_page)
        if len(found_in_headlines) == max_hits:
            break
    return found_in_headlines


def get_tip_for_stock(stock_ticker):
    url = f"https://www.barchart.com/stocks/quotes/{stock_ticker}"
    r = requests.get(url, headers={'User-Agent': 'Custom'})
    txt = r.content.decode("utf-8")
    if '''class="buy-color"''' in txt:
        rating = "buy"
    elif '''class="hold-color"''' in txt:
        rating = "hold"
    elif '''class="sell-color"''' in txt:
        rating = "sell"
    else:
        rating = "Dont know"
    print(rating)


def get_stock_tips():
    tips = []
    stock_watchlist = load_stock_watchlist()
    stock_mentions = search_headline_pages_for_stock_mentions(stock_watchlist)
    for stock_mention in stock_mentions:
        tip = get_tip_for_stock(stock_mention)
        if tip != "Don't know":
            tips.append((stock_mention, tip))
    return tips


if __name__ == '__main__':
    stock_tips = get_stock_tips()
    print(stock_tips)
