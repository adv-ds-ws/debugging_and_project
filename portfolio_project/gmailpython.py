import smtplib, ssl
import os

from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from email import encoders

import imaplib
import email


class Server:
    def __init__(self, sender_email= "python.workshop.coba@gmail.com", password = "advisori123"):
        self.port = 465  # For SSL
        self.password = password
        self.sender_email = sender_email
        self.smtp_server = "smtp.gmail.com"
        
        self.context = ssl.create_default_context()
        
    def send_email(self, receiver_email, subject, body, attachment_dir = None):
        message = MIMEMultipart()
        message["Subject"] = subject
        message["From"] = self.sender_email
        message["To"] = receiver_email
        
        message.attach(MIMEText(body, "plain"))
        
        if attachment_dir:
            if isinstance(attachment_dir, str):
                message = self.add_attachment(message, attachment_dir)
            elif isinstance(attachment_dir, list):
                for i_att in attachment_dir:
                    message = self.add_attachment(message, i_att)

        with smtplib.SMTP_SSL(self.smtp_server, self.port, context=self.context) as server:
            server.login(self.sender_email, self.password)
            server.sendmail(self.sender_email, receiver_email, message.as_string())
            
    def get_emails(self, mail_filter="ALL"):
        mail_list = []
        
        mail = imaplib.IMAP4_SSL("smtp.gmail.com")
        mail.login(self.sender_email, self.password)

        mail.select('inbox')
        
        type, data = mail.search(None, mail_filter) #can be set to 'ALL' in order to get all emails
        mail_ids = data[-1]
        id_list = mail_ids.split()
        
        for i_id in id_list:
            i_email = {}
            typ, data = mail.fetch(i_id, '(RFC822)')
            
            for response_part in data:
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1].decode("utf-8"))
            
            i_email['id'] = int(i_id)
            i_email['from'] = msg['from']
            i_email['subject'] = msg['subject']
            
            for part in msg.walk():
                if part.get_content_type() == "text/plain": # ignore attachments/html
                    body = part.get_payload(decode=True)
                    i_email['body'] = body.decode('cp1250')
                    
            i_email['attachments'] = []
            for part in msg.walk():
                if part.get_content_maintype() in ['application', 'image']: # ignore attachments/html
                    attachment = {}
                    attachment['name'] = part.get_filename()
                    attachment['payload'] = part.get_payload(decode=True)
                    i_email['attachments'].append(attachment)
            
            mail_list.append(i_email)  
        return mail_list
    
    def add_attachment(self, message, attachment_dir):
        attachment = MIMEBase("application", "octet-stream")

        with open(attachment_dir, "rb") as file:
            attachment.set_payload(file.read())

        encoders.encode_base64(attachment)

        attachment.add_header(
            "Content-Disposition",
            f"file; filename= {os.path.basename(attachment_dir)}",
        )
        message.attach(attachment)
        
        return message
