import os
from portfolio_project.gmailpython import Server
from portfolio_project.get_current_prices import write_current_prices_for_all_stocks_in_portfolios_to_db

# Get Emails #
server = Server()
mail_list = server.get_emails()

mail = mail_list[0]
print("FROM: " + mail['from'])
print("SUBJECT: " + mail['subject'])
print("BODY: \"" + mail['body'] + "\"")

# Send Email #
subject = "Automatic mailing"
body = "The text of the new mail"
attachment_file = os.sep.join(["portfolios", "port1.xlsx"])

receiver = ""  # example@example.com
server.send_email(receiver_email=receiver,
                  subject=subject,
                  body=body,
                  attachment_dir=[attachment_file])

# Get current prices to database #
write_current_prices_for_all_stocks_in_portfolios_to_db()
