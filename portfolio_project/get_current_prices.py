import sqlite3
import pandas as pd
import warnings
import glob
from datetime import date, timedelta

from pandas_datareader import data
from pandas_datareader._utils import RemoteDataError
# https://pydata.github.io/pandas-datareader/stable/remote_data.html


def df_from_ticker(ticker, start_date, end_date):
    try:
        df = data.DataReader(ticker, 'yahoo', start_date, end_date).reset_index()
        df["symbol"] = ticker
        return df
    except RemoteDataError:
        warnings.warn("symbol {} not found".format(ticker))
        return


def write_current_prices_for_all_stocks_in_portfolios_to_db():
    portfolio_files = glob.glob('portfolios/port*.xlsx')

    portfolios = pd.concat([pd.read_excel(p) for p in portfolio_files])
    symbols = set(portfolios.Symbol)

    d_format = "%Y-%m-%d"
    e_date = date.today().strftime(d_format)
    s_date = (date.today() - timedelta(days=365)).strftime(d_format)

    stocks = pd.concat(df_from_ticker(sym, s_date, e_date) for sym in symbols)

    con = sqlite3.connect('stocks.db')
    stocks.to_sql("stocks", con, schema=None, if_exists='replace', index=False)


if __name__ == '__main__':
    write_current_prices_for_all_stocks_in_portfolios_to_db()
