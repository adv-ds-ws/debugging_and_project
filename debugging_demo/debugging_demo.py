import copy
import datetime


# With side effects
def sort_transactions_desc_by_date1(transactions, dates):
    transactions_sorted = []
    dates_sorted = []
    for i in range(len(transactions)):
        min_date = max(dates)
        min_index = dates.index(min_date)
        dates_sorted.append(min_date)
        transactions_sorted.append(transactions[min_index])
        del dates[min_index]
        del transactions[min_index]
    return transactions_sorted


# Without side effects
def sort_transactions_desc_by_date2(transactions, dates):
    transactions_sorted = []
    dates_sorted = copy.deepcopy(dates)
    for i in range(len(dates_sorted)):
        min_date = max(dates_sorted[i:])
        min_index = dates.index(min_date)
        dates_sorted[min_index] = dates_sorted[i]
        dates_sorted[i] = min_date
        transactions_sorted.append(transactions[min_index])
    return transactions_sorted


# functional
def sort_transactions_desc_by_date3(transactions, dates):
    transactions_and_dates = list(zip(transactions, dates))
    transactions_and_dates.sort(key=lambda x: x[1], reverse=True)
    sorted_transactions, sorted_dates = zip(*transactions_and_dates)
    return sorted_transactions


def get_date_and_value_of_first_overdraft1(transactions, dates):
    transactions_and_dates = list(zip(transactions, dates))
    transactions_and_dates.sort(key=lambda x: x[1])
    running_total = 0
    for amount, date in transactions_and_dates:
        running_total += amount
        if running_total < 0:
            return running_total, date


# functional
def sort_transactions_desc_by_date4(transactions, dates):
    transactions_and_dates = list(zip(transactions, dates))
    transactions_and_dates.sort(key=lambda x: x[1], reverse=True)
    sorted_transactions, sorted_dates = zip(*transactions_and_dates)
    return sorted_transactions


if __name__ == '__main__':
    transaction_list = [70, -1010, 20, 200, 2000, -750, -1100, 250, 170, ]
    date_list = [datetime.date(2017, 11, 20),
                 datetime.date(2017, 12, 2),
                 datetime.date(2017, 12, 10),
                 datetime.date(2018, 1, 16),
                 datetime.date(2017, 10, 1),
                 datetime.date(2017, 10, 2),
                 datetime.date(2017, 10, 4),
                 datetime.date(2017, 10, 3),
                 datetime.date(2017, 11, 5)]
    sort_transactions_desc_by_date1(transaction_list, date_list)
    sort_transactions_desc_by_date2(transaction_list, date_list)
    sort_transactions_desc_by_date3(transaction_list, date_list)
    get_date_and_value_of_first_overdraft1(transaction_list, date_list)
    sort_transactions_desc_by_date4(transaction_list, date_list)
